## Checklist

1.  Membuat Halaman Chat Box
    1. [X] Tambahkan _lab_6.html_ pada folder templates

	bisa diakses di pada lab_6.html (alhamdulillah bisa)
	
    2. [x] Tambahkan _lab_6.css_ pada folder _./static/css_

	bisa digunakan pada lab_6.html (alhamdulillah bisa)
	
    3. [x] Tambahkan file _lab_6.js_ pada folder _lab_6/static/js_

	bisa digunakan ketika ada event yang harus menggunakan (alhamdulillah bisa)
	
    4. [x] Lengkapi potongan kode pada _lab_6.js_ agar dapat berjalan

	bisa membuat fungsinya (saat pake javaScript ada masalah tapi pake jquery tidak)

2. Mengimplementasikan Calculator
    1. [X] Tambahkan potongan kode ke dalam file _lab_6.html_ pada folder templates

	bisa diakses ketika lab_6.html dipanggil
	
    2. [x] Tambahkan potongan kode ke dalam file _lab_6.css_ pada folder _./static/css_

	bisa digunakan pada lab_6.html
	
    3. [x] Tambahkan potongan kode ke dalam file _lab_6.js_ pada folder _lab_6/static/js_

	bisa mengimplementasikan code ketika ada event
	
    4. [x] Implementasi fungsi `AC`.

	bisa mengimplementasikan event AC untuk menghapus isi di kalkulator

3.  Mengimplementasikan select2
    1. [x] Load theme default sesuai selectedTheme

	sudah ada default theme ketika pertamakali membuka lab_6.html
	
    2. [x] Populate data themes dari local storage ke select2

	sudah ada data themes dari local ke select2
	
    3. [x] Local storage berisi themes dan selectedTheme

	sudah tersimpan themes dan selectedTheme ke dalam storage 
	
    4. [x] Warna berubah ketika theme dipilih

	sudah dapat terimlementasikan pengubahan warna

4.  Pastikan kalian memiliki _Code Coverage_ yang baik
    1. [X]  Jika kalian belum melakukan konfigurasi untuk menampilkan _Code Coverage_ di Gitlab maka lihat langkah `Show Code Coverage in Gitlab` di [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md). 

    sudah tertampil pada redme.md di awal
	
	2. [x] Pastikan _Code Coverage_ kalian 100%.

	sudah 100%

###  Challenge Checklist
1. Latihan Qunit
    1. [ ] Implementasi dari latihan Qunit
1. Cukup kerjakan salah satu nya saja:
    1. Implementasikan tombol enter pada chat box yang sudah tersedia
        1. [ ] Buatlah sebuah _Unit Test_ menggunakan Qunit
        2. [ ] Bulah fungsi yang membuat _Unit Test_ Tersebut _passed_ 
    1. Implementasikan fungsi `sin`, `log`, dan `tan`. (HTML sudah tersedia di dalam potongan kode)
        1. [ ] Buatlah sebuah _Unit Test_ menggunakan Qunit
        2. [ ] Bulah fungsi yang membuat _Unit Test_ Tersebut _passed_